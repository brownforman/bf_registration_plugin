<?php

/**
 * Plugin Name: B-F Registration Plugin
 * Description: Generic registration form to save consumer details. Usage: <code>[bf-registration-form]</code>
 * Author:      Jeff
 * Author URI:
 * Version:     1.0
 * Text Domain: bf_registration_form
 *
 * @package   B-F Registration Form
 * @version   1.0
 * @author    Arthur/Norberto
*/

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) exit;

// Global object
$bf_registration_form = new BF_Registration_Form();

/**
 * The main Template class.
 *
 * @since 0.1
 */
class BF_Registration_Form {

	/**
	 * Sets up everything.
	 *
	 * @since 0.1
	 * @access public
	 */
	public function __construct() {
		$this->setup_globals();
		$this->includes();
		$this->setup_actions();
	}

	private function testing($answer) {
		return;
	}

	/**
	 * Sets up the global vars.
	 *
	 * @since 0.1
	 * @access private
	 */
	private function setup_globals() {
		// Directories and URLs
		$this->file       = __FILE__;
		$this->basename   = plugin_basename( $this->file );
		$this->plugin_dir = plugin_dir_path( $this->file );
		$this->plugin_url = plugin_dir_url( $this->file );
		$this->lang_dir   = trailingslashit( $this->plugin_dir . 'languages' );

		$this->admin_url = $this->plugin_url . 'admin';
		$this->admin_dir = $this->plugin_dir . 'admin';

		$this->lib_url = $this->plugin_url . 'lib';
		$this->lib_dir = $this->plugin_dir . 'lib';
	}

	/**
	 * Requires files for the plugin
	 *
	 * @since 0.1
	 * @access private
	 */
	private function includes() {
		// This file defines all of the common functions
		require( $this->plugin_dir . 'functions.php' );
		// If in the admin panel, set up the admin functions
		if ( is_admin() ) {
			require( $this->admin_dir . '/admin.php' );
			require( $this->admin_dir . '/settings.php' );
		}
	}

	/**
	 * Sets up the actions, filters and wp hooks.
	 *
	 * @since 0.1
	 * @access private
	 */
	private function setup_actions() {
		// Load the text domain for i18n
		add_action( 'init', array( $this, 'load_textdomain' ) );
		// Load the default and custom styles
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Load the text domain.
	 *
	 * @since 0.1
	 * @access public
	 */
	public function load_textdomain() {
		$locale = get_locale();
		$locale = apply_filters( 'plugin_locale',  $locale, 'bf_registration_form' );
		$mofile = sprintf( 'bf_registration_form-%s.mo', $locale );

		$mofile_local  = $this->lang_dir . $mofile;
		$mofile_global = WP_LANG_DIR . '/bf_registration_form/' . $mofile;

		if ( file_exists( $mofile_local ) ) {
			return load_textdomain( 'bf_registration_form', $mofile_local );
		}
		if ( file_exists( $mofile_global ) ) {
			return load_textdomain( 'bf_registration_form', $mofile_global );
		}
		return false;
	}

	/**
	 * Custom styles
	 *
	 * @since 0.1
	 * @access public
	 */
	public function enqueue_styles() {
		wp_enqueue_style( 'bfrf-styles', $this->plugin_url . 'public/css/app.css' );
	}

	/**
	 * Custom styles
	 *
	 * @since 0.1
	 * @access public
	 */
	public function enqueue_scripts() {
		////Standard
		wp_register_script( 'bfrf-app', $this->plugin_url . 'public/js/app.js', array('jquery'), NULL, true );

		////Early Times
		//wp_register_script( 'bfrf-app', $this->plugin_url . 'public/js/app.js', array('jquery_js'), NULL, true );
	}

}