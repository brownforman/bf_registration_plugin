function bfrf_admin_country_async($) {
	'use strict';
	$.ajax({
		url: "http://bf-webservice.herokuapp.com/api/countries",
		dataType: "json",
		type : "GET",
		success : function(r) {
			var html = "";
			var elem = $('#_bfrf_country');
			var default_iso = (elem.attr('data-default-iso') !== 'undefined' && elem.attr('data-default-iso') !== '') ? elem.attr('data-default-iso') : "us";
			$.each(r, function(key, value){
				if(value.iso == default_iso){
					html += "<option value='"+value.iso+"' selected='selected'>"+value.name+"</option>\n";
				} else{
					html += "<option value='"+value.iso+"'>"+value.name+"</option>\n";
				}
			});
			elem.html(html);
		}
	});
}

jQuery(document).ready(function(){
	'use strict';
	let $ = jQuery.noConflict();
	bfrf_admin_country_async($);
});