window.bfrf_g_recaptcha_expired = () => {
	'use strict';
	jQuery("#g_recaptcha").addClass('invalid');
	jQuery('#bfrf_form [type=submit]').prop('disabled', 'disabled');
}

window.bfrf_g_recaptcha_success = () => {
	'use strict';
	jQuery("#g_recaptcha").removeClass('invalid');
	jQuery('#bfrf_form [type=submit]').removeProp('disabled');
}

window.open_lightboxes = [];

function bfrf_alert(msg) {
	'use strict';
	$ = jQuery.noConflict();
	$('#bfrf-error-lightbox #bfrf_lightbox_message').html(msg);
	$('#bfrf-error-lightbox').lightbox_me({
		centered: true,
		closeClick: false,
		closeEsc: false,
		onLoad: function(){ window.open_lightboxes.push('#bfrf-error-lightbox'); }
	});

	$('#bfrf-error-lightbox #bfrf_lightbox_close_button').click(function(){
		$('#bfrf-error-lightbox').trigger('close');
	});

	$('#bfrf-error-lightbox #bfrf_lightbox_close_button, #bfrf-error-lightbox .bfrf-lightbox-button').show();	//
}

function bfrf_close_lightboxes($){
	'use strict';
	$(open_lightboxes).each(function(){
		$(this).trigger('close');
	});
}

function bfrf_country_async($) {
	'use strict';
	return $.ajax({
		url: "http://bf-webservice.herokuapp.com/api/countries",
		dataType: "json",
		type : "GET",
		success : function(r) {
			var html = "";
			var elem = $('#bfrf_form #country');
			var default_iso = (elem.attr('data-default-iso') !== 'undefined' && elem.attr('data-default-iso') !== '') ? elem.attr('data-default-iso') : "us";
			$.each(r, function(key, value){
				if(value.iso == default_iso){
					html += "<option value='"+value.iso+"' selected='selected'>"+value.name+"</option>\n";
				} else{
					html += "<option value='"+value.iso+"'>"+value.name+"</option>\n";
				}
			});
			elem.html(html);
		}
	});
}

function bfrf_country_change($) {
	'use strict';
	let elem = $('#bfrf_form #country');
	elem.on('change', function(){
		let handle = '#bfrf_form [data-iso="'+$(this).val()+'"]';
		let state = $(handle);

		$('.state').attr("disabled", "disabled").hide().find('select, input').val('');

		if (state.length == 0) {
			state = $('#bfrf_form [data-iso="none"]');
		}
		state.removeAttr("disabled").show();
	}).change();
}

function bfrf_create_consumer($, form_data, form){
	'use strict';
	return $.ajax({
		url: "http://bf-webservice.herokuapp.com/api/createConsumer",
		method: "POST",
		data: form_data,
		beforeSend: function() {
			form.find('.bf-error').hide();
		},
		statusCode:{
			400: function(data){
				bfrf_alert("There was an error with your submission. Please try again. (400)");
			},
			500: function(data) {
				bfrf_alert("There was an error with your submission. Please try again. (500)");
			}
		}
	});
}

function bfrf_create_consumer_handler($, form_data, form) {
	'use strict';
	$.when(bfrf_create_consumer($, form_data, form)).then(function(data){
		//Success
		if (data) {
			form_data['consumer_id'] = data;
			$.when(bfrf_insert_promotion_response($, form_data, form)).then(function(data){
				if (form.find('[name=send_user_email]').val()=="true") {
					bfrf_send_user_email_handler($, form_data);
				} else {
					bfrf_close_lightboxes($);
					bfrf_thank_you($);
				}
			}).fail(function(err){
				//Error creating consumer info
				bfrf_close_lightboxes($);
				bfrf_alert("Error inserting promotion response. Please try again.");
			});
		}
	}).fail(function(err){
		//Error creating consumer info
		bfrf_close_lightboxes($);
		bfrf_alert("Error creating consumer info. Please try again.");
	});
}

function bfrf_email_confirm_check(){
	'use strict';

	$ = jQuery.noConflict();
	if ($('#bfrf_form #confirm_email').val().trim() !== $('#bfrf_form #email').val().trim()) {
		$('#bfrf_form #confirm_email').addClass('invalid');
		$('#bfrf_form #confirm_email')[0].setCustomValidity("E-mail addresses must match.");
		return false;
	}

	$('#bfrf_form #confirm_email').removeClass('invalid');
	$('#bfrf_form #confirm_email')[0].setCustomValidity("");
	return true;
}

function bfrf_form_submit($) {
	'use strict';
	$('#bfrf_form [type=submit]').click(function(e){
		$('#bfrf_form').addClass('validated');
	});

	$('#bfrf_form').on('submit', function(e){
		e.preventDefault();

		let form = $('#bfrf_form');
		let form_data = bfrf_serialize_array($);
		form_data["action"] = "bf_register";
		if ( "confirm_email" in form_data && !bfrf_email_confirm_check()) {
			return;
		}

		let recaptcha = "";
		if ($("#g_recaptcha").length > 0) {
			recaptcha = $("#g-recaptcha-response").val();
	   		if(recaptcha == ""){
	   			$("#g_recaptcha").addClass('invalid');
	   			return;
	   		} else {
	   			$("#g_recaptcha").removeClass('invalid');
	   		}

	   		form_data['g-recaptcha-response'] = recaptcha;
		}

		var obj = bfrf_loading_object($);
		obj.then(function(){
			$.when(bfrf_validatelda($, form_data)).then(function(data){
				if (data){
					bfrf_create_consumer_handler($, form_data, form)
						/*
						bfrf_close_lightboxes($);
						let msg = "There was an error with your submission. Please try again.";
						if (err.responseText == "g-recaptcha-required") {
							msg = "reCAPTCHA is required. Please try again.";
						} else if (err.responseText == "g-recaptcha-failed") {
							msg = "Please re-try the reCAPTCHA.";
						}
						bfrf_alert(msg);
						return;
 						*/
				} else {
					//under lda
					bfrf_close_lightboxes($);
					bfrf_scrollTo("bf-error");
				}
			}).fail(function(err){
				//invalid date
				bfrf_close_lightboxes($);
				bfrf_scrollTo("bf-error");
			});

		});

	});

}

function bfrf_insert_promotion_response($, form_data, form) {
	'use strict';
	if (!form_data['consumer_id'] || form_data['consumer_id'] == "") {
		return;
	}
	let ajaxes = [];
	form.find('.bfrf-promotion-response').each(function(){
		let question = $(this).attr('data-question');
		let target = $(this).attr('data-target');
		let answer = "";

		if ($(this).attr('data-type') == 'radio') {
			answer = $('input[name="'+target+'"]:checked').val();
		} else if ($(this).attr('data-type') == 'checkbox') {
			answer = $('#'+target).is(':checked') ? $('#'+target).val() : $('#'+target).attr('data-not-checked');
		} else {
			answer = $('#'+target).val();
		}

		let fd = {
			brand_id: form_data['brand_id'],
			consumer_id: form_data['consumer_id'],
			source_code: form_data['source_code'],
		    question: question,
		    answer: answer,
		    promotion_url: $('#bfrf_form #permalink').val()
		};

		ajaxes.push(
			$.ajax({
				url: "http://bf-webservice.herokuapp.com/api/insertPromotionResponse",
				method: "POST",
				data: fd
			})
		);
	});

	return $.when.apply($, ajaxes);
}

function bfrf_loading_object($) {
	'use strict';

	let promise = new Promise(function(resolve, reject){
		let msg = `<i class="fa fa-spinner fa-pulse"></i><span class="sr-only">Sending...</span>`;
		$('#bfrf-loading-lightbox #bfrf_lightbox_message').html(msg);
		$('#bfrf-loading-lightbox').lightbox_me({
				centered: true,
				closeClick: false,
				closeEsc: false,
				onLoad: function(){
					window.open_lightboxes.push('#bfrf-loading-lightbox');
					resolve(1);
			}
		});
	});
	return promise;
}

function bfrf_scrollTo(hash) {
    location.hash = "#" + hash;
}

function bfrf_send_user_email($, form_data) {
	'use strict';
	form_data["action"] = "send_user_email";
	return $.ajax({
		url: ajax_url,
		method: "POST",
		data: form_data
	});
}

function bfrf_send_user_email_handler($, form_data) {
	'use strict';
	$.when(bfrf_send_user_email($, form_data)).then(function(data){
		bfrf_close_lightboxes($);
		bfrf_thank_you($);
	}).fail(function(err){
		/*

		If you want to show an error message, do it here

		 */
		bfrf_close_lightboxes($);
		bfrf_thank_you($);
	});
}

function bfrf_serialize_array($) {
	'use strict';
	let form_data = {};
	let form = $('#bfrf_form');
	$.each($('#bfrf_form').serializeArray(), function(k, v){
		form_data[v.name] = v.value;
	});
	form.find('input:checkbox:not(:checked)').each(function(){
		form_data[$(this).attr('id')] = "N";
	});
	return form_data;
}

function bfrf_state_async($) {
	'use strict';
	$.ajax({
		url: 'http://bf-webservice.herokuapp.com/api/us/states',
		dataType: "json",
		type : "GET",
		success : function(r) {
			let html = `<option value="" selected="selected">State</option>`;
			let elem = $('#bfrf_form select#us_state_abbrv');
			$.each(r, function(key, value){
				html += "<option value='"+value.abbrev+"'>"+value.name+"</option>\n";
			});
			elem.html(html);
		}
	});

	$.ajax({
		url: 'http://bf-webservice.herokuapp.com/api/au/states',
		dataType: "json",
		type : "GET",
		success : function(r) {
			let html = `<option value="" selected="selected">Province</option>`;
			let elem = $('#bfrf_form select#state_prov_terr');
			$.each(r, function(key, value){
				html += "<option value='"+value.abbrev+"'>"+value.name+"</option>\n";
			});
			elem.html(html);
		}
	});
}

function bfrf_thank_you($) {
	'use strict';
	window.location = $('#bfrf_form #permalink').val()+"?success=yes";
	return;
}

function bfrf_validatelda($, form_data){
	'use strict';
	//validate LDA
	return $.ajax({
		url: "http://bf-webservice.herokuapp.com/api/validatelda",
		method: "POST",
		data: {
			"day":form_data["bday"],
			"month":form_data["bmonth"],
			"year":form_data["byear"],
			"country":form_data["country"],
			"category":"spirit"
		},
		beforeSend: function(){
			$('.bf-error').hide();
		},
		success: function(data){
			if (!data){
				//underlda
				$('.lda-error').show();
			}
		},
		error: function(err){
			$('.date-error').show();
		}
	});
}

jQuery(document).ready(function(){
	'use strict';
	let $ = jQuery.noConflict();
	$.when(bfrf_country_async($)).then(function(){
		bfrf_state_async($);
		bfrf_country_change($);
	})

	if ( $('#bfrf_form #confirm_email').length > 0 ) {
		$('#bfrf_form #confirm_email').change(function(){
			bfrf_email_confirm_check();
		});
	}

	bfrf_form_submit($);
});

/* CUSTOM JS BELOW HERE, ADD FUNCTION CALL TO DOC READY, WILL MAKE UPDATES EASIER TO IMPLEMENT */
/* IF CHANGES MUST BE DONE TO CODE ABOVE, BE SURE TO DIFF NEW FILE WITH YOUR CHANGES */